import { Test, TestingModule } from '@nestjs/testing';
import { GeneralCqrsPoliciesService } from './generalcqrs-policies.service';

describe('GeneralCqrsPoliciesService', () => {
  let service: GeneralCqrsPoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GeneralCqrsPoliciesService],
    }).compile();

    service = module.get<GeneralCqrsPoliciesService>(GeneralCqrsPoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
