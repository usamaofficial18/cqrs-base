import { Module, HttpModule } from '@nestjs/common';
import { GeneralCqrsAggregatesManager } from './aggregates';
import { GeneralCqrsEntitiesModule } from './entity/entity.module';
import { GeneralCqrsQueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { GeneralCqrsCommandManager } from './command';
import { GeneralCqrsEventManager } from './event';
import { GeneralCqrsController } from './controllers/generalcqrs/generalcqrs.controller';
import { GeneralCqrsPoliciesService } from './policies/generalcqrs-policies/generalcqrs-policies.service';

@Module({
  imports: [GeneralCqrsEntitiesModule, CqrsModule, HttpModule],
  controllers: [GeneralCqrsController],
  providers: [
    ...GeneralCqrsAggregatesManager,
    ...GeneralCqrsQueryManager,
    ...GeneralCqrsEventManager,
    ...GeneralCqrsCommandManager,
    GeneralCqrsPoliciesService,
  ],
  exports: [GeneralCqrsEntitiesModule],
})
export class GeneralCqrsModule { }
