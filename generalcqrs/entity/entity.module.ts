import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GeneralCqrs } from './generalcqrs/generalcqrs.entity';
import { GeneralCqrsService } from './generalcqrs/generalcqrs.service';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [TypeOrmModule.forFeature([GeneralCqrs]), CqrsModule],
  providers: [GeneralCqrsService],
  exports: [GeneralCqrsService],
})
export class GeneralCqrsEntitiesModule { }
