import { GeneralCqrsAddedCommandHandler } from "./generalcqrs-added/generalcqrs-added.handler";
import { GeneralCqrsRemovedCommandHandler } from "./generalcqrs-removed/generalcqrs.removed.handler";
import { GeneralCqrsUpdatedCommandHandler } from "./generalcqrs-updated/generalcqrs-updated.handler";

export const GeneralCqrsEventManager = [GeneralCqrsAddedCommandHandler, GeneralCqrsRemovedCommandHandler, GeneralCqrsUpdatedCommandHandler];
