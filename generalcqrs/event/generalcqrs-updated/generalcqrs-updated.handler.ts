import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { GeneralCqrsUpdatedEvent } from './generalcqrs-updated.event';
import { GeneralCqrsService } from '../../entity/generalcqrs/generalcqrs.service';

@EventsHandler(GeneralCqrsUpdatedEvent)
export class GeneralCqrsUpdatedCommandHandler implements IEventHandler<GeneralCqrsUpdatedEvent> {
  constructor(private readonly object: GeneralCqrsService) { }

  async handle(event: GeneralCqrsUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne({ uuid: updatePayload.uuid },
      { $set: updatePayload });
  }
}
