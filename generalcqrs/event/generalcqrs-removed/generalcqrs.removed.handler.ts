import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { GeneralCqrsService } from '../../entity/generalcqrs/generalcqrs.service';
import { GeneralCqrsRemovedEvent } from './generalcqrs-removed.event';

@EventsHandler(GeneralCqrsRemovedEvent)
export class GeneralCqrsRemovedCommandHandler implements IEventHandler<GeneralCqrsRemovedEvent> {
  constructor(private readonly generalCqrsService: GeneralCqrsService) { }
  async handle(event: GeneralCqrsRemovedEvent) {
    const { generalCqrs } = event;
    await this.generalCqrsService.deleteOne({ uuid: generalCqrs.uuid });
  }
}
