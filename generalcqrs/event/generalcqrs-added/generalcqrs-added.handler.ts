import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { GeneralCqrsAddedEvent } from './generalcqrs-added.event';
import { GeneralCqrsService } from '../../entity/generalcqrs/generalcqrs.service';

@EventsHandler(GeneralCqrsAddedEvent)
export class GeneralCqrsAddedCommandHandler implements IEventHandler<GeneralCqrsAddedEvent> {
  constructor(private readonly generalCqrsService: GeneralCqrsService) { }
  async handle(event: GeneralCqrsAddedEvent) {
    const { generalCqrs } = event;
    await this.generalCqrsService.create(generalCqrs);
  }
}
