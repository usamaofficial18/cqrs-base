import {
  Injectable,
  BadRequestException,
  NotImplementedException,
  HttpService,
} from '@nestjs/common';
import {
  GeneralCqrsWebhookDto,
} from '../../entity/generalcqrs/generalcqrs-webhook-dto';
import { GeneralCqrsService } from '../../../generalcqrs/entity/generalcqrs/generalcqrs.service';
import { GeneralCqrs } from '../../../generalcqrs/entity/generalcqrs/generalcqrs.entity';
import { from, throwError } from 'rxjs';
import { switchMap, map, retry } from 'rxjs/operators';
import { FRAPPE_API_GET_GENERAL_ENDPOINT } from '../../../constants/routes';
import { GENERAL_ALREADY_EXISTS } from '../../../constants/messages';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import * as uuidv4 from 'uuid/v4';
@Injectable()
export class GeneralCqrsWebhookAggregateService {
  constructor(
    private readonly generalCqrsService: GeneralCqrsService,
    private readonly settingsService: SettingsService,
    private readonly http: HttpService,
    private readonly clientTokenManager: ClientTokenManagerService,
  ) { }

  generalCqrsCreated(generalCqrsPayload: GeneralCqrsWebhookDto) {
    return from(
      this.generalCqrsService.findOne({
        name: generalCqrsPayload.name,
      }),
    ).pipe(
      switchMap(generalCqrs => {
        if (generalCqrs) {
          return throwError(new BadRequestException(GENERAL_ALREADY_EXISTS));
        }
        const provider = this.mapGeneralCqrs(generalCqrsPayload);
        provider.uuid = uuidv4();
        provider.isSynced = false;
        this.generalCqrsService
          .create(provider)
          .then(success => { })
          .catch(error => { });
        return this.syncGeneralChild(provider);
      }),
    );
  }

  mapGeneralCqrs(generalCqrsPayload: GeneralCqrsWebhookDto) {
    const generalCqrs = new GeneralCqrs();
    Object.assign(generalCqrs, generalCqrsPayload);
    return generalCqrs;
  }

  syncGeneralChild(generalCqrsPayload: GeneralCqrs) {
    return this.settingsService.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(new NotImplementedException());
        }
        return this.clientTokenManager.getClientToken().pipe(
          switchMap(token => {
            const url =
              settings.authServerURL +
              FRAPPE_API_GET_GENERAL_ENDPOINT +
              generalCqrsPayload.name;
            return this.http
              .get(url, {
                headers: this.settingsService.getAuthorizationHeaders(token),
              })
              .pipe(
                map(res => res.data.data),
                switchMap(response => {
                  const childTable = this.mapChildTable(response.guardians);
                  return from(
                    this.generalCqrsService.updateOne(
                      { name: generalCqrsPayload.name },
                      {
                        $set: {
                          childtable: childTable,
                          isSynced: true,
                        },
                      },
                    ),
                  );
                }),
              );
          }),
          retry(3),
        );
      }),
    );
  }

  mapChildTable() {
    const sanitizedData = [];
    return sanitizedData;
  }

  generalCqrsDeleted(generalCqrsPayload: GeneralCqrsWebhookDto) {
    this.generalCqrsService.deleteOne({ name: generalCqrsPayload.name });
  }

  generalCqrsUpdated(generalCqrsPayload: GeneralCqrsWebhookDto) {
    return from(
      this.generalCqrsService.findOne({ name: generalCqrsPayload.name }),
    ).pipe(
      switchMap(generalCqrs => {
        if (!generalCqrs) {
          return this.generalCqrsCreated(generalCqrsPayload);
        }
        generalCqrs.isSynced = true;
        this.generalCqrsService.updateOne(
          { name: generalCqrs.name },
          { $set: generalCqrsPayload },
        );
        return this.syncGeneralChild(generalCqrs);
      }),
    );
  }
}
