import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveGeneralCqrsListQuery } from './retrieve-generalcqrs-list.query';
import { GeneralCqrsAggregateService } from '../../aggregates/generalcqrs-aggregate/generalcqrs-aggregate.service';

@QueryHandler(RetrieveGeneralCqrsListQuery)
export class RetrieveGeneralCqrsListQueryHandler
  implements IQueryHandler<RetrieveGeneralCqrsListQuery> {
  constructor(private readonly manager: GeneralCqrsAggregateService) { }
  async execute(query: RetrieveGeneralCqrsListQuery) {
    const { offset, limit, search, sort, clientHttpRequest } = query;
    return await this.manager.getGeneralCqrsList(
      Number(offset),
      Number(limit),
      search,
      sort,
      clientHttpRequest,
    );
  }
}
