import { IQuery } from '@nestjs/cqrs';

export class RetrieveGeneralCqrsQuery implements IQuery {
  constructor(public readonly uuid: string, public readonly req: any) {}
}
