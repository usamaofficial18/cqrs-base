import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveGeneralCqrsQuery } from './retrieve-generalcqrs.query';
import { GeneralCqrsAggregateService } from '../../aggregates/generalcqrs-aggregate/generalcqrs-aggregate.service';

@QueryHandler(RetrieveGeneralCqrsQuery)
export class RetrieveGeneralCqrsQueryHandler
  implements IQueryHandler<RetrieveGeneralCqrsQuery> {
  constructor(private readonly manager: GeneralCqrsAggregateService) { }

  async execute(query: RetrieveGeneralCqrsQuery) {
    const { req, uuid } = query;
    return this.manager.retrieveGeneralCqrs(uuid, req);
  }
}
