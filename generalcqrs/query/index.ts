import { RetrieveGeneralCqrsQueryHandler } from './get-generalcqrs/retrieve-generalcqrs.handler';
import { RetrieveGeneralCqrsListQueryHandler } from './list-generalcqrs/retrieve-generalcqrs-list.handler';

export const GeneralCqrsQueryManager = [
  RetrieveGeneralCqrsQueryHandler,
  RetrieveGeneralCqrsListQueryHandler,
];
