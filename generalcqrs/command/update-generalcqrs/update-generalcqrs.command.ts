import { ICommand } from '@nestjs/cqrs';
import { UpdateGeneralCqrsDto } from '../../entity/generalcqrs/update-generalcqrs-dto';

export class UpdateGeneralCqrsCommand implements ICommand {
  constructor(public readonly updatePayload: UpdateGeneralCqrsDto) { }
}
