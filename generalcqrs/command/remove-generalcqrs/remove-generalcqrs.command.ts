import { ICommand } from '@nestjs/cqrs';

export class RemoveGeneralCqrsCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
