import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveGeneralCqrsCommand } from './remove-generalcqrs.command';
import { GeneralCqrsAggregateService } from '../../aggregates/generalcqrs-aggregate/generalcqrs-aggregate.service';

@CommandHandler(RemoveGeneralCqrsCommand)
export class RemoveGeneralCqrsCommandHandler
  implements ICommandHandler<RemoveGeneralCqrsCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: GeneralCqrsAggregateService,
  ) { }
  async execute(command: RemoveGeneralCqrsCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.remove(uuid);
    aggregate.commit();
  }
}
