import { ICommand } from '@nestjs/cqrs';
import { GeneralCqrsDto } from '../../entity/generalcqrs/generalcqrs-dto';

export class AddGeneralCqrsCommand implements ICommand {
  constructor(
    public generalCqrsPayload: GeneralCqrsDto,
    public readonly clientHttpRequest: any,
  ) { }
}