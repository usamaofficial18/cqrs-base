import { AddGeneralCqrsCommandHandler } from "./add-generalcqrs/add-generalcqrs.handler";
import { RemoveGeneralCqrsCommandHandler } from "./remove-generalcqrs/remove-generalcqrs.handler";
import { UpdateGeneralCqrsCommandHandler } from "./update-generalcqrs/update-generalcqrs.handler";

export const GeneralCqrsCommandManager = [AddGeneralCqrsCommandHandler, RemoveGeneralCqrsCommandHandler, UpdateGeneralCqrsCommandHandler];
